package com.jflyfox.client;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jflyfox.autocreate.template.CRUD;
import com.jflyfox.autocreate.util.AutoCreate;
import com.jflyfox.autocreate.util.DbUtils;
import com.jflyfox.util.StrUtils;

import java.sql.SQLException;
import java.util.Map;

public class AutoCreateClientDemo {

	public static void main(String[] args) throws Exception {
		run();
	}


	protected static void run() throws SQLException, Exception {
		init();

		// 模块名
		String module = "system";
		// 包名
		String packagePath = "com.channelsoft.datacenter.module.admin";
		// 生成的表，支持逗号分割
		String tables = "sys_city_info,tb_data_quality,tb_enterprise,tb_outfile,tb_require,tb_require_area,tb_rule,tb_rule_quality";
		// 模板路径
		String templatePath = "/autopath/template/project/soft_admin/";

		Map<String, CRUD> crudMap = null;
		if (StrUtils.isEmpty(tables) || "all".equalsIgnoreCase(tables)) {
			crudMap = DbUtils.getCRUDMap();
		} else {
			String[] tableArray = tables.split(",");
			crudMap = DbUtils.getCRUDMap(tableArray);
		}

		// System.setProperty("user.dir","D:\\workspace\\datacenter\\wyx_component\\autocreate");
		new AutoCreate().setTemplatePath(templatePath).setPackagePath(packagePath).setModule(module).setCrudMap(crudMap).create();
	}

	public static void init() {

		String jdbcUrl = "jdbc:mysql://10.130.29.221:3306/data_collection?characterEncoding=UTF-8&zeroDateTimeBehavior=convertToNull";
		String user = "root";
		String password = "123456";
		String driverClass = "com.mysql.jdbc.Driver";

		System.out.println("####jdbcUrlRead:" + jdbcUrl);
		System.out.println("####user:" + user);
		System.out.println("####password:" + password.trim());
		System.out.println("####driverClass:" + driverClass);

		C3p0Plugin c3p0Plugin = new C3p0Plugin(jdbcUrl, user, password.trim(), driverClass);

		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		c3p0Plugin.start();
		arp.start();
	}

}
