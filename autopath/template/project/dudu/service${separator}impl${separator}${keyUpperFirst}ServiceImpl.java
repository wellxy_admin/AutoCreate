package @{packagePath}.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jflyfox.dudu.component.base.BaseServiceImpl;
import com.jflyfox.dudu.component.model.Query;
import org.springframework.stereotype.Service;
import @{packagePath}.model.@{crud.table.className};
import @{packagePath}.dao.@{strutils.toUpperCaseFirst(crud.urlKey)}Mapper;
import @{packagePath}.service.I@{strutils.toUpperCaseFirst(crud.urlKey)}Service;

/**
 * @{crud.table.remarks} 服务层口层
 *
 * @author flyfox 369191470@qq.com on @{now}.
 */
@Service
public class @{strutils.toUpperCaseFirst(crud.urlKey)}ServiceImpl extends BaseServiceImpl<@{strutils.toUpperCaseFirst(crud.urlKey)}Mapper, @{crud.table.className}> implements I@{strutils.toUpperCaseFirst(crud.urlKey)}Service {

    public PageInfo<@{crud.table.className}> select@{strutils.toUpperCaseFirst(crud.urlKey)}Page(Query query) {
        PageHelper.startPage(query.getPage(), query.getRows());
        PageInfo<@{crud.table.className}> pageInfo = new PageInfo<@{crud.table.className}>(baseMapper.select@{strutils.toUpperCaseFirst(crud.urlKey)}Page(query));
        return pageInfo;
    }
}