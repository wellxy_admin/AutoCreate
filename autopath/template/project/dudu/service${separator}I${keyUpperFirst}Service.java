package @{packagePath}.service;

import com.github.pagehelper.PageInfo;
import com.jflyfox.dudu.component.base.IBaseService;
import com.jflyfox.dudu.component.model.Query;
import @{packagePath}.model.@{crud.table.className};

/**
 * @{crud.table.remarks} 服务接口层
 *
 * @author flyfox 369191470@qq.com on @{now}.
 */
public interface I@{strutils.toUpperCaseFirst(crud.urlKey)}Service extends IBaseService<@{crud.table.className}> {

    /**
     * 分页查询
     *
     * @param query
     * @return
     */
    PageInfo<@{crud.table.className}> select@{strutils.toUpperCaseFirst(crud.urlKey)}Page(Query query);

}