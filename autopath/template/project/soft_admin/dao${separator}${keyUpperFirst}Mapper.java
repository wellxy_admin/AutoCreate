package @{packagePath}.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.channelsoft.datacenter.component.model.Query;
import @{packagePath}.model.@{crud.table.className};
import org.apache.ibatis.annotations.Param;
import com.channelsoft.datacenter.commons.util.StrUtils;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.jdbc.SQL;

import java.util.List;

/**
 * @{crud.table.remarks} 数据层
 *
 * @author flyfox 369191470@qq.com on @{now}.
 */
public interface @{strutils.toUpperCaseFirst(crud.urlKey)}Mapper extends BaseMapper<@{crud.table.className}> {

    @SelectProvider(type = SqlBuilder.class, method = "select@{strutils.toUpperCaseFirst(crud.urlKey)}Page")
    List<@{crud.table.className}> select@{strutils.toUpperCaseFirst(crud.urlKey)}Page(Query query);

    class SqlBuilder {
        public String select@{strutils.toUpperCaseFirst(crud.urlKey)}Page(Query query) {
            #
            var str = "";
            for(column in crud.table.columns){
                    if(columnLP.index!=1) {
                        str = str + ",";
                    }
                    str = str + "t." + strutils.toLowerCase(column.columnName);
                    if(strutils.toLowerCase(column.columnName)!=strutils.toLowerCaseFirst(column.columnJavaName) ) {
                        str = str + " as " + strutils.toLowerCaseFirst(column.columnJavaName);
                    }
                }
            #
            String sqlColumns = "@{str}";
            return new SQL() {{
                SELECT(sqlColumns +
                        " ,uu.username as updateName,uc.username as createName");
                FROM(" @{crud.table.tableName} t ");
                LEFT_OUTER_JOIN(" sys_user uu on t.update_id = uu.id ");
                LEFT_OUTER_JOIN(" sys_user uc on t.create_id = uc.id ");
                if (StrUtils.isNotEmpty(query.getStr("name"))) {
                    WHERE(" t.name = \#{name}");
                }
                if (StrUtils.isNotEmpty(query.getOrderBy())) {
                    ORDER_BY(query.getOrderBy());
                } else {
                    ORDER_BY(" t.id desc");
                }
            }}.toString();
        }
    }
}
