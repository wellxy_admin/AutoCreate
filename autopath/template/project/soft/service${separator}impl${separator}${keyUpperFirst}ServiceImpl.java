package @{packagePath}.service.impl;

import org.springframework.stereotype.Service;
import @{packagePath}.po.@{crud.table.className};
import @{packagePath}.mapper.@{strutils.toUpperCaseFirst(crud.urlKey)}Mapper;
import @{packagePath}.service.I@{strutils.toUpperCaseFirst(crud.urlKey)}Service;
import com.channelsoft.ccuas.common.exception.ServiceException;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * @{crud.table.remarks} 服务层口层
 *
 * @author flyfox 369191470@qq.com on @{now}.
 */
@Service
public class @{strutils.toUpperCaseFirst(crud.urlKey)}ServiceImpl implements I@{strutils.toUpperCaseFirst(crud.urlKey)}Service {

    @Autowired
    private @{strutils.toUpperCaseFirst(crud.urlKey)}Mapper mapper;

    public @{crud.table.className} getOne(Integer id) throws ServiceException {
        return mapper.selectOne(id);
    }

    public List<@{crud.table.className}> getList(PageInfo page, @{crud.table.className} form) throws ServiceException {
        if(page != null){
            PageHelper.startPage(page.getPageNum(), page.getPageSize());
        }
        return mapper.selectList(form);
    }

    public int add(@{crud.table.className} model)throws ServiceException {
        return mapper.insert(model);
    }

    public int update(@{crud.table.className} model)throws ServiceException {
        return mapper.update(model);
    }

    public int delete(Integer id) throws ServiceException {
        return mapper.delete(id);
    }
}