字段名|变量名|类型|示例值|说明|必填
结果|code|int|0|成功：0，失败：其他	是
消息|message|String|获取数据成功|消息提示，失败可展示|否
数据|data|String|Json数组|主要用于返回广告信息|是
# for(column in crud.table.columns){ #
@{column.remarks}|@{strutils.toLowerCaseFirst(column.columnJavaName)}|@{column.javaType}||@{column.remarks}|是
# } #

---------------------------------
{
# for(column in crud.table.columns){ #
    "@{strutils.toLowerCaseFirst(column.columnJavaName)}":"",
# } #
}

-----------------------------
#
for(column in crud.table.columns){
    var str = "";
    if(columnLP.index!=1) {
      str = str + ",";
    }
    str = str + strutils.toLowerCase(column.columnName);
    if(strutils.toLowerCase(column.columnName)!=strutils.toLowerCaseFirst(column.columnJavaName) ) {
      str = str + " as " + strutils.toLowerCaseFirst(column.columnJavaName);
    }
    print(str);
}
println("");
#

